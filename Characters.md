# Characters

## Pepper

![Pepper](https://www.peppercarrot.com/data/wiki/medias/img/chara_pepper.jpg)

* **Age**: 18 (11 at start of comic, around 14-17 between episodes 2 through 20)
* **Place**: Her house, in the forest of Squirrel's End (a little farmer's village).
* **Familiar**: Carrot, a male ginger cat, curious and always looking for fun and entertainment.
* **Hobby**: Studying magical rules; an inner paradox between trying to understand the mysteries of Chaosah and accepting what's already there.
* **Theme**: Particles, galaxies, black-holes, total-mess, deconstruction, emptiness, fullness, infinity, paradox.
* **Family**: Orphaned, educated at the Squirrel's End orphanage.
* **Motivation**: Find respect, love and friends by being the best witch.
* **Miscellaneous**: Pepper is left-handed but uses her wand right-handed. This is because Chicory, the founder of Chaosah, was right-handed and wrote all of the rituals down right-handed. So all Chaosah witches learn to use their right hand for casting spells.
* **Magic**: Pepper practices **Chaosah** magic, the base magic of the chaos. She is the sole living member of Chaosah (The other Chaosah witches were re-animated by Zombiah after their deaths in The Great War). Her tools: entropy, gravity, time paradox, space deformations. She has the fabric of the universe law in her hands. She finds reversing time for a second or creating miniature black holes easier than creating fire or healing someone. She can also switch multi-verses and dimensions. Summoning demons or creatures from other worlds is possible to her.
* **Known potion/Spell**: Potion of Flight (ep01),
Potion of growing/shrinking size for her and Carrot (ep05),
Summoning three demons of Chaosah (book1, *Incantations for Demons of Chaosah*) (ep08),
Laughing Potion (ep12),
Mega-hair-growth Potion (ep12),
"Bright-side" Potion (ep12),
Smoke Potion (ep12),
Stink bubbles (ep12),
Summoning a Chaosahn black hole (book7, *Gravitational fields*) (ep12),
Micro dimension (ep24)

**History**: 

Pepper was raised at the Little Acorns Orphanage in the village of Squirrel's End. At an early age, she showed an aptitude for magic and was enrolled in the Hippiah school of magic. While she excelled at magic, she also excelled at driving her instructors batty with various traps, potions, and other hijinks. When her instructors insisted she make a patch of flowers bloom she found the whole exercise pointless and boring. For Pepper, it was literally like watching grass grow.

In one instance, Pepper was so frustrated with her classmates and teacher making fun of her inability to grow flowers that she let off a blast of Rea in frustration. This drew the attention of the remaining Chaosah witches who were in search of their new heir. Pepper was reluctant to go with the Chaosah Witches, but she realized the Hippiah school was an imperfect fit. Her Hippiah instructors felt they had little choice but to let Pepper go, as Pepper was not performing well as a Hippiah witch. They also did not want the Chaosah Witches around, since the Chaosah witches had a reputation of bringing the ire of Ah with them. Since Pepper had very little money and no living relatives, it only took a little prodding for the villagers to help erect Pepper a house in the forest of Squirrel's End. The alternative, they feared, would be for her to live in the village proper.

Being a witch of Chaosah is not particularly lucrative, and several times Pepper has not had enough Ko to afford even modest ingredients for her potions. It wasn't until the recent potion challenge (ep3) that Pepper had any kind of money, and had to augment her broom with additional potions and Rea for flight (Komonan wood brooms require much less Rea to fly, and fetch a hefty price). Even with her considerable skill, there have been a few times where she's had to make in-flight adjustments.

## Carrot

![Carrot](https://www.peppercarrot.com/data/wiki/medias/img/chara_carrot.jpg)

* **Age**: Unknown
* **Magic**: Unknown
* **Place**: Forest of Squirrel's End (along with Pepper)
* **Hobby**: Sleeping
* **Motivations**: Sleeping, Eating, Companionship with Pepper

**History**

Carrot is a male orange striped tabby cat. He was alone around Squirrel's End looking for food when he spied Pepper having trouble with her Hippiah lessons (ep18). He hid amongst the carrots in a food storage room, which prompted Pepper to name him "Carrot". He is Pepper's constant companion (and occasional potion-tester).

## Saffron

![Saffron](https://www.peppercarrot.com/data/wiki/medias/img/chara_saffron.jpg)

* **Age**: 18 (around 14-17 between episodes 2 through 20)
* **Magic**: Magmah
* **Place**: Komona, the magic floating tree city, a place of markets and rich people.
* **Familiar**: Truffel, a white angora cat.
* **Hobby**: Working to be more rich and famous.
* **Theme**: Urban, metal, gems, forge, glass, mirrors, shining, rocks, caves.
* **Family**: Orphaned, educated in the Komona orphanage.
* **Motivations**: Being the best witch and restoring the prestige of her magic school, 'Magmah'.
* **First Episode**: ep01
* **Known potion/Spell**: Potion of Poshness (ep06), Fire spell (ep22)

**History**:

Saffron demonstrated an aptitude for magic at an early age. When she was 6, she managed to counterfeit a handful of Ko so closely that even the head of the mint could barely tell the difference. Rather than punish her (where she could become a formidable criminal), it was decided to put her into the advanced Magmah classes where she could perfect her craft. She learned quickly and realized she was pages ahead of her teachers. Rather than continue her formal education, she instead used her talents to open "Saffron Witchcraft" located in the highest-rent district of Komona's markets. Her primary clientèle ranges from merchants looking to improve their wares and the alchemists of Zombiah researching materials. Saffron's business thrives; so much so that a steady queue of people form outside of her office looking for her advice. Even the Ko mint looked to her to see how best to improve the Ko coins so they were harder to counterfeit. She has risen to prominence as one of the youngest entrepreneurs of Komona.

## Truffel

![Truffel](https://www.peppercarrot.com/data/wiki/medias/img/chara_truffel.jpg)

* **Age**: Unknown
* **Magic**: Unknown
* **Place**: Komona, with Saffron
* **Hobby**: Sleeping, grooming
* **Motivations**: Sleeping, playing with Carrot when he comes to visit
* **First Episode**: ep01

Truffel is a female white angora cat. She lives with Saffron in Komona and travels with her wherever Saffron goes. She has been seen playing with Carrot whenever Pepper and Carrot come to visit Saffron.

## Shichimi

![Shichimi](https://www.peppercarrot.com/data/wiki/medias/img/chara_shichimi.jpg)

* **Age**: 18 (around 14-17 between episodes 2 through 20)
* **Magic**: Ah
* **Place**: Land of the Setting Moons; living a nomadic lifestyle in this territory.
* **Familiar**: Yuzu, a young two tailed magical fox (kitsune).
* **Hobby**: Observing life around, being aware of her role within life.
* **Theme**: Other worlds, mystic, ghost, life, regeneration, grow, energy.
* **Motivations**: To live a quiet life and die in peace, guided by the unknowable winds.
* **Family**: Selected by Ah.
* **Clothing**: Shichimi wears a [hair-piece](https://www.peppercarrot.com/data/wiki/medias/img/shichimi_hair_piece.jpg) fashioned from the bone of one of the dragons near the temples of Ah. What it signifies and how she obtained it is known only to her. Her garments are a simple robe and wrap, similar to a kimono.
* **First Episode**: ep06
* **Known potion/Spell**: Potion of Giant Monster (ep06), Blinding light (ep22)

## Yuzu

![Yuzu](https://www.peppercarrot.com/data/wiki/medias/img/chara_yuzu.jpg)

* **Age**: Unknown
* **Magic**: Unknown
* **Place**: Beside Shichimi as she travels throughout Hereva
* **Hobby**: Sleeping, grooming
* **Motivations**: Loyalty to Shichimi and fiercely protecting her should she be in danger.
* **First Episode**: ep06

Yuzu is a two-tailed orange fox (kitsune) that travels beside Shichimi.

## Coriander

![Coriander](https://www.peppercarrot.com/data/wiki/medias/img/chara_coriander.jpg)

* **Age**: 18 (around 14-17 between episodes 2 through 20)
* **Magic**: Zombiah
* **Place**: Qualicity, an industrial city from the Technologist Union.
* **Familiar**: Mango, a black cockerel.
* **Hobby**: Geek who enjoys bringing life to non-living beings.
* **Theme**: Industry, voodoo, necromancy (reanimation of non-living beings), robotics, maker.
* **Family**: Princess of Qualicity. Spoiled heavily by her father, the king.
* **Motivation**: Inventing a new kind of magic based on logical and physical law, allowing all people to use magic, not just witches.
* **First Episode**: ep06
* **Known potion/Spell**: Potion of Zombification (ep06), Skeleton summoning (ep22)

## Mango

![Mango](https://www.peppercarrot.com/data/wiki/medias/img/chara_mango.jpg)

* **Age**: Unknown
* **Magic**: Unknown
* **Place**: Qualicity; next to Coriander
* **Hobby**: Unknown
* **Motivations**: Secretly desires to rule the world, but as of yet has not made concrete plans on how to achieve this.
* **First Episode**: ep06

Mango is a black cockerel that is often seen with Coriander. 

## Spirulina

![Spirulina](https://www.peppercarrot.com/data/wiki/medias/img/chara_spirulina.jpg)

* **Age**: 14
* **Magic**: Aquah
* **Place**: Unknown
* **Familiar**: Durian, a male betta fish.
* **Hobby**: Unknown
* **Theme**: Mysterious practitioner of Aquah
* **Family**: Unknown
* **Motivation**: Unknown
* **First Episode**: ep21
* **Known potion/Spell**: Forceful ejection from water (ep21), Water Tentacles (ep22)

Spirulina is the mysterious witch-representative of Aquah. She appears at the second magic contest, much to the surprise of Pepper and the audience. Few have seen the members of Aquah since the great war, so little is known about what drove Aquah to send Spirulina to this contest, or what Spirulina wants to gain by participating.

## Durian

![Durian](https://www.peppercarrot.com/data/wiki/medias/img/chara_durian.jpg)

* **Age**: Unknown
* **Magic**: Unknown
* **Place**: Aquah, next to Spirulina
* **Hobby**: Unknown
* **Motivations**: Unknown
* **First Episode**: ep21

Durian is a red male betta spelendens fish.

## Camomile

![Camomile](https://www.peppercarrot.com/data/wiki/medias/img/chara_camomile.jpg)

* **Age**: 18 (around 14-17 between episodes 2 through 20)
* **Magic**: Hippiah
* **Place**: Squirrel's End
* **Familiar**: Unknown
* **Hobby**: Unknown
* **Theme**: Practitioner of Hippiah magic. Growth. Nurturing.
* **Family**: Unknown
* **Motivation**: Unknown
* **First Episode**: ep18
* **Known potion/Spell**: Growth spell (ep18, ep22)

Camomile is one of the witches from Pepper's class at the "School of Hippiah Witchcraft". She is gifted in her practice of Hippiah magic. She is human with raccoon ears and tail. Early-on she was teased for her strange appearance, but her skills with Hippiah magic endeared her to her teachers and classmates. Camomile is driven to be the best Hippiah witch ever, and her formidable talents and innate magical abilities make her an exceptional Hippiah witch.

## Fairies

* **Age**: Presumed infinite
* **Magic**: Unknown, although presumed to be a variant of **Chaosah** magic.
* **Place**: Magic fairy grot, forest of Squirrel's End.
* **Hobby**: Appearing around and granting wish to strangers.
* **First Episode**: ep05


## Thyme

![Thyme](https://www.peppercarrot.com/data/wiki/medias/img/chara_thyme.jpg)

* **Age**: (presumed to be around 116 but she tends to lie about her age)
* **Magic**: **Chaosah**
* **Description**: The small and wise leader of the Chaosah witches.
* **First Episode**: ep11
* **Known potion/Spell**: Tranforming herself into the Prince Acren (ep11), Reorganizing particules to rebuild the house (ep25) 

**History**: 

Thyme is the current wise leader of the Chaosah Witches. She rules the remaining witches with her iron fist (even though it is somewhat arthritic). She tends to be calculating and sneaky and has been known to sneak off to a cave to look at "The Web". Her mind is sharp and she is always calculating for the best way to bring the defeated Chaosah back to their rightful and feared prominence over the other Herevan magical schools.

Cumin is Thyme's apprentice, though Thyme feels more comfortable with Cayenne's council. Thyme frequently bosses Cumin around and sends her on silly errands and dangerous quests with little regard for Cumin's well-being. Some Chaosah witches wondered that if Thyme had her way she would make Cayenne her apprentice. They postulated these missions were an attempt to terminate Cumin's apprenticeship, since apprenticeships are for life.

## Cayenne

![Cayenne](https://www.peppercarrot.com/data/wiki/medias/img/chara_cayenne.jpg)

* **Age**: 60s (late 50s from episodes 2-20)
* **Magic**: **Chaosah**
* **Description**: The tall, thin and rigid witch of Chaosah. Spell teacher for Pepper.
* **First Episode**: ep11
* **Known potion/Spell**: Throwing fire with her eyes (ep11), Spell cancellation (ep12), Reducing house into particules (ep25)

**History**:

Cayenne is the most visible and foreboding of the Chaosah witches. She commands respect and is not one to take orders from anyone other than Thyme. During the Great War, she was the most feared Chaosah witch, casting spells and causing devastation wherever she roamed. Little is known of her past, which suits Cayenne just fine. Her life is dedicated to Chaosah and to protecting Thyme.


## Cumin

![Cumin](https://www.peppercarrot.com/data/wiki/medias/img/chara_cumin.jpg)

* **Age**: late 60s (early 60s from episodes 2-20)
* **Magic**: **Chaosah**
* **Description**: A bon-vivant witch of Chaosah. Teacher of potions and potion ingredients for Pepper.
* **First Episode**: ep11
* **Known potion/Spell**: Transformation into a bat (ep11, ep18), Invocation of a tornado butterfly

**History**: 

Cumin started off as an apprentice of Thyme. Despite being in her sixties and an accomplished witch, she still suffers under the apprentice relationship; being bossed around almost instinctively by the older witch. Cumin's specialty is creating potions and she teaches Pepper how to craft potent potions. Perhaps the reason Pepper creates potions over other means of incantation relates to how approachable Cumin is, compared to the other witches of Chaosah.

Cumin is only somewhat aware of her strained relationship with Thyme as her apprentice. She became suspicious after an errand to Aquah where she nearly drowned in the attempt (Cumin complained that Thyme hadn't informed her that Aquah was so far under water, which Thyme said should have been obvious to even the dimmest of witches).


## Mayor of Komona

![Mayor of Komona](https://www.peppercarrot.com/data/wiki/medias/img/chara_mayor.jpg)
* **Age**: late 40s
* **First Episode**: ep06
* **Description**: The current mayor of Komona.

**History**

Mayor Bramble, the current mayor of Komona, is the 70th mayor of Komona. His administration rose to prominence with his campaign promises to return Komona as the center of commerce and magical prowess to Komona. Part of this was to institute an annual potion contest to pick out the brightest and best magical talent throughout Hereva. Unfortunately, the results of the potion contest (ep06) caused great concern for the residents of Komona. Whether the potion contest will return (and whether Mayor Bramble will be re-elected) is still to be determined.

## Prince Acren

![Prince Acren](https://www.peppercarrot.com/data/wiki/medias/img/chara_acren.jpg)

* **Age**: 18 (14 in episode 11)
* **First Episode**: ep11 (not him, but his appearance)
* **Description**: Prince, and as-yet-uncrowned king of Acren. Orphaned after the death of his parents, The King and Queen of Acren (ep11).

## The Sage
![The Sage](https://www.peppercarrot.com/data/wiki/medias/img/chara_sage.jpg)
* **Age**: Unknown
* **First Episode**: ep16
* **Description**: The mysterious and wise teacher of Hereva.

**History**

The Sage is the great and wise teacher of Hereva. Located in the hot springs of Hereva, The Sage dispenses wisdom to those who seek it. He sits in the hot springs, and speaks his wisdom to anyone who will hear. Those who understand the message of The Sage (and have the ability to stay in the water long enough without pruning), can truly say they have received his wisdom. Many who made the trek to see The Sage report the only wisdom they received was sounds of contentment and a promise that he will teach them when he gets out of the bath. Few have seen The Sage outside of his bath, and fewer still speak openly of his wisdom. As a result, The Sage spends most of his time alone in the bath sighing with contentment; his wisdom unheard and unheeded.


## Vanilla / Diva Capsica

* **Age**: 60?
* **Magic**: **Magmah**
* **Description**: Actual master of Magmah; pink hair, a bit chubby, pink lace scarf with frills (for parties); dream to organize the most impressive and beautiful fire show of all times; can be friendly and ridiculously passionate about her dream, or very violent if someone tries to upset her, or to touch her students, with no mercy: she loves to propose magic fights to solve any problem.
* **First Episode**: None
* **Known potion/Spell**: Fire Torrent, Build Flames
* **Hobbies**: Researches about impressive fire spells  

## Unknown Mermaid

* **Age**: Undetermined, probably very old
* **Magic**: **Aquah**
* **Description**: Actual master of Aquah; Giant mermaid with long and punky hair, with a gold trident and a tail with shining scales, as hard as dragons'. Probably: nobody can affirm she really exists. This description was made from little fragments listened here and there through out history.
* **First Episode**: None
* **Known potion/Spell**: Unknown

## Apiaceae

* **Age**: Deceased at a very advanced age
* **Magic**: **Zombiah**
* **Description**: Found the Zombiah school after discovering the Zombification Process. Chief engineer of Qualicity, then Queen of Qualicity, Coriander grandmother, Soumbala's creator. Coriander's outfit (+ hat) belonged to her grandmother when she was young. Black skin, curly long hair.
* **First Episode**: None
* **Known potion/Spell**: Zombification Process, Growth Retarder (accident)

**History**:   

Apiaceae discovered the Zombification Process while trying to resurrect her little mouse Anis. She founded Zombiah to develop all the potential of this process. As she loved tinkering in her own side, she used the Zombification Process to boost her creations. She contributed to mix magic and technology to bring important changes and progress in Hereva. She founded the Technologist-Union, and participated to the build of countless structures, as the Cathedral of Qualicity. She often had to treat with the king of Qualicity, to finally became his wife, to one condition: he had to lead alone. The duties of a queen didn't interest her (even if she helped to take important decisions), and was too much work with Master of Zombiah and Chief Engineer. That's why their marriage remained secret during a long time.   

She had to treat with Ah about the Zombification Process on living beings, used for a long time on formerly living beings as working force, which was an ethical problem. Apiaceae never really listened to the explanation of witches of Ah about spirit true form. She accepted to curtail the use of the process on working forces (replaced by artificial ones), but experiences on living beings were still practiced. The war was the straw that broke the camel's back: she saw that her creation could be used at wrong purposes, and accepted to really limit the practice of the Zombification Process to non-human beings by signing a treaty, that only her and a few people know the exact content. She also decided to transform her long work about the most perfect robot into a wise robot, called Soumbala, to take her place as master of Zombiah, to teach wisdom to students who wanted to learn this magic after her death. Her son, the actual king of Qualicity, often tells that when he was young, he often saw his mother working in a hiding place on secret projects, that she didn't want to leave after the war until her death. But after, this hiding place was empty.  

One day, she accidentally created a potion that slowed her own life. She never said how it was created to anyone, even her son ; the only thing we know is that the Zombification Process was one key element of the potion. Her son's wife was her last assistant, and tried to find the other elements, so hardly it became an obsession. But one day, after an explosion, the king went to his wife laboratory... but she had disappeared. It is an episode that Coriander hates to tell.  
We can find trace of her passage everywhere in Qualicity, as she signed all her creations with her coat.  


## Soumbala

* **Age**: Unknown. Activated just after the great war.
* **Magic**: **Zombiah**
* **Description**: Actual Master of Zombiah ; a masterful robot creation designed and animated by Apiaceae ; looks like a fakir with a turban ; always floats, the legs crossed. Contains all the wisdom of Apiaceae after the war (and maybe more?)
* **First Episode**: None
* **Known potion/Spell**: Zombification Process

**History**:   

Originally planned to be the robot with the best intelligence, near to the human's one, Apiaceae decided to adapt her masterpiece to have the needed wisdom to teach the Zombification Process and its application after her death, in agreement with the last treaty she signed with Ah. Soumbala became the Master who taught Coriander all she knows about Zombiah. It is also the first advisor of the king, as it can help with its infinite wisdom. Rumors says that Soumbala contains the last secrets of Apiaceae (like her Growth Retarder potion), but Coriander never found any secret into Soumbala. Yet...  


## Botanic

* **Age**: 75
* **Magic**: **Hippiah**
* **Description**: Former Master of Hippiah ; Specialized in herbal medicine. Gave the power to her apprentice Basilic before leaving, trying to found the "Perfect Garden" Floreco.
* **First Episode**: None
* **Known potion/Spell**: 

**History**:   

Botanic was not known to be a good witch. She was lazy, rarely went outside the greenhouse her master gave her to teach her knowledge. Basilic often had to do the teacher to her classmates, learning by herself as she was brilliant. She went to ask to Botanic only if it was a need (her hygiene was so terrible that flowers and tree started to grow on her!). The Botanic only known talent was the creation of powerful medicine from the plants she spent all her day to study. When Basilic complained about her duty as a master, Botanic always answered than the nature is the real master, and witches of Hippiah just have to listen to its lessons. Basilic thought that was a pityful excuse to be lazy. Botanic often talked about Floreco, the Perfect Garden where the nature can give to witches who deserved it, fairly, without any work or any violence. Basilic never listened to what she called "her green delirium", except the "any violence" part, she started to think about seriously.   

One day, Basilic didn't find her master in the greenhouse, only one message: "I left to find Floreco. You are the master, now. Quick, go teaching!", which angered Basilic so much that she destroyed the greenhouse by accident. She took advantage of this accident to build the actual Hippiah structure, offering true equipments and accomodations for the students, comfortable enough to forget how she had to live (in a hut she built at the top of a tree, as Botanic has never built anything for her students). To keep the students' faith in Hippiah, Basilic always lied about Botanic, saying that she was a powerful and respected witch, too focused on her experiences to have the time to teach (which is half a lie, even if Botanic spent more time to do nothing than working!). She continues to lie after being the Master.  

## Basilic

* **Age**: 34
* **Magic**: **Hippiah**
* **Description**: Actual Master of Hippiah ; apprentice of Botanic ; very brillant, learnt almost all her knowledge by herself ; a little arrogant.
* **First Episode**: Unknown
* **Known potion/Spell**: 

**History**:   

Basilic was the apprentice of Botanic, but had to learn by herself (see Botanic history). She is very brilliant, and loves to teach to others (with a little arrogant behaviour: she doesn't hesitate to mock someone if they are very lame). When she became Master of Hippiah, she built accommodations to avoid that her students have to live in the trees (even if they had to know how to survive in a forest).  
She took Pepper as a student when she was young ; but after an explosion provoked by Pepper, she accepted to give her to the Chaosah witches, regarding her poor performance with the Hippiah magic reported by her teachers.  
She hates conflict and war (Hippiah didn't take part of the war), and works for a more peaceful world. She also dislikes the Technologist Union, as they seem not to care about life or environment. Chaosah and Magmah scare her a bit, considering the environment disaster they are able to provoke...  

## Quassia

![Quassia](https://www.peppercarrot.com/data/wiki/medias/img/chara_basilic.jpg)

* **Age**: Unknown
* **Magic**: **Hippiah**
* **Description**: Teacher of Hippiah
* **First Episode**: ep18
* **Known potion/Spell**: 

**History**:   

## Millet

* Age: Deceased
* **Magic**: **Hippiah**
* **Description**: Beloved former teacher of Hippiah
* **First Episode**: Unknown
* **Known potion/Spell**: 

**History**:

Millet was the beloved teacher of Hippiah magic. Part opossum, part human, she gave wisdom, encouragement, and love to all of the students of Hippiah. She was not loved by the faculty of Hippiah, who thought that she was too easy on her students, and spoiled them with attention.


## Torreya

![Torreya](https://www.peppercarrot.com/data/wiki/medias/img/chara_torreya.jpg)


* **Age**: 19/20
* **Magic**: **Ah**
* **Description**: Pilot of the dragon Arra
* **First Episode**: ep35
* **Known potion/Spell**: 

**History**

Torreya is from the generations of students that came to Ah after the Great War. She showed an aptitude early on for communicating with the dragons of Ah. Her allegiance is more toward the dragons than to Ah, but she endures her Ah training as long as it allows her to spend time with the dragons.
